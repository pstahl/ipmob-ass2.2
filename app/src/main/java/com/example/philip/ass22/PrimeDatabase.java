package com.example.philip.ass22;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Philip on 2015-02-07.
 */
public class PrimeDatabase extends SQLiteOpenHelper {

    private static final String PRIME = "prime";
    private static final String DATABASE_NAME = "primedb";
    private static final int DATABASE_VERSION = 2;
    private static final String TABLE_NAME = "prime";
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    PRIME + " INTEGER PRIMARY KEY AUTOINCREMENT);";

    public PrimeDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void savePrime(Long prime) {
        Log.d("savePrime", "Inserting into database");
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIME, prime);
        database.insert(TABLE_NAME, null, values);
        database.close();
    }

    public Long loadPrime() {
        SQLiteDatabase database = getReadableDatabase();
        String query = "SELECT " + PRIME + " FROM " + TABLE_NAME + ";";
        Cursor cur = database.query(TABLE_NAME, new String[]{ PRIME}, null, null, null, null, null);
        Long prime;
        if (cur.moveToNext()) {
            prime = cur.getLong(0);
        } else {
            prime = new Long(0);
        }
        database.close();

        return prime;
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d("Creating Database", "\n\n\n");
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        return;
    }

}
